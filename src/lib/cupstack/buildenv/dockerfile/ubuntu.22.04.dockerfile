# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2021-2021, Jayesh Badwaik <jayesh@badwaik.dev>
# --------------------------------------------------------------------------------------------------

FROM ubuntu:22.04
ADD cupstack.buildenv /bin/cupstack.buildenv
RUN /bin/cupstack.buildenv --os ubuntu.22.04
