# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2021-2021, Jayesh Badwaik <jayesh@badwaik.dev>
# --------------------------------------------------------------------------------------------------

FROM almalinux:8
ADD cupstack.buildenv /bin/cupstack.buildenv
RUN /bin/cupstack.buildenv --os almalinux.8
