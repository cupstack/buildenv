# Build Environment for Compilers


The objective of this repository is to setup the build environment for for
different linux distributions.

## Instructions

1. Download the build environment script

```
curl
```

2. Run the script using the OS names from the table below

```bash
./cupstack.buildenv --os <os-name>
```

| Distribution                  | Supported? |
|-------------------------------| ---------- |
| **AlmaLinux**                 |            |
| almalinux.8                   |     ✅     |
| almalinux.9                   |     ✅     |
| **ArchLinux**                 |            |
| archlinux.latest              |     ✅     |
| **RockyLinux**                |            |
| rockylinux.8                  |     ✅     |
| rockylinux.9                  |     ✅     |
| **Ubuntu**                    |            |
| ubuntu.22.04                  |     ✅     |

